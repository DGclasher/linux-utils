# Nextcloud Setup

## Description
Setup nextcloud in your own server

## Setup
Download the script
```
wget https://gitlab.com/DGclasher/linux-utils/-/raw/main/nextcloud/setup.sh
```
Change permissions and execute
```
chmod +x setup.sh && ./setup.sh
```

Note: For http_encrypt, you would need a domain name.