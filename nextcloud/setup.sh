#!/bin/bash

nextcloud_dir="$HOME/nextcloud"
proxy_dir="$HOME/.local/share/nginxproxymanager"

if [ -d "$nextcloud_dir" ]; then
    echo "Already exists.."
    exit 1
fi

dependencies() {
    sudo apt update
    sudo apt install docker.io -y
    sudo usermod -aG docker "$(whoami)"
    if ! command -v docker-compose; then
        sudo wget "https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)" -O /usr/local/bin/docker-compose
        sudo chmod +x /usr/local/bin/docker-compose
    fi
}
dependencies

https_encrypt() {
    mkdir -p "$proxy_dir/data"
    mkdir "$proxy_dir/letsencrypt"
    wget "https://gitlab.com/DGclasher/linux-utils/-/raw/main/nextcloud/files/nginxproxymanager.yml" -O "$proxy_dir/docker-compose.yml"
    wget "https://gitlab.com/DGclasher/linux-utils/-/raw/main/nextcloud/files/.env.proxy" -O "$proxy_dir/.env"
    echo -e "Go to $proxy_dir and change the .env, then start by docker-compose up -d\nAdd SSL certs"
}

nextcloud_install() {
    mkdir -p "$nextcloud_dir/data"
    mkdir "$nextcloud_dir/sqldata"
    wget "https://gitlab.com/DGclasher/linux-utils/-/raw/main/nextcloud/files/nextcloud.yml" -O "$nextcloud_dir/docker-compose.yml"
    wget "https://gitlab.com/DGclasher/linux-utils/-/raw/main/nextcloud/files/.env.nextcloud" -O "$nextcloud_dir/.env"
}
nextcloud_install

while(true); do
    read -rp "Want to install https_encrypt (y/n): " option
    if [ "$option" == "y" ]; then
        echo "Installing https encrypt"
        https_encrypt
        break
    elif [ "$option" == "n" ]; then
        break
    else
        echo "Invalid choice"
    fi
done

echo "Done!!"
echo -e "Go to $nextcloud_dir, change passwords in .env and do sudo docker-compose up -d\nVisit http://localhost:8081 to see the interface"