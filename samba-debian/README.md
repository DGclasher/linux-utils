# Samba Debian

## Description
Setup samba share on debian and based distros.

### Run
+ Clone the repo
+ Give execute permissions to `setup.sh`
  ```
  chmod +x setup.sh
  ```
+ Execute Script
  ```
  sudo ./setup.sh
  ```

+ Note: If you want private share, uncomment the `private` section on `smb.conf`.
