# Vaultwarden

## Description
A self hosted password manager.

## Setup
Download the setup script
```
wget https://gitlab.com/DGclasher/linux-utils/-/raw/main/vaultwarden/setup.sh
```
Change permissions and execute
```
chmod +x setup.sh && ./setup.sh
```

Note: You would need a domain name to make this work.