#!/bin/bash

vault_dir="$HOME/.local/share/vaultwarden"

if [ -d "$vault_dir" ]; then
    echo "Already exists.."
    exit 1
fi

dependencies() {
    sudo apt update
    sudo apt install docker.io -y
    sudo usermod -aG docker "$(whoami)"
    if ! command -v docker-compose; then
        sudo wget "https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)" -O /usr/local/bin/docker-compose
        sudo chmod +x /usr/local/bin/docker-compose
    fi
}
dependencies

setup_vaultwarden() {
    mkdir -p "$vault_dir/data"
    wget https://gitlab.com/DGclasher/linux-utils/-/raw/main/vaultwarden/files/vaultwarden.yml -O "$vault_dir/docker-compose.yml"
    wget https://gitlab.com/DGclasher/linux-utils/-/raw/main/vaultwarden/files/.env.vault -O "$vault_dir/.env"
    echo -e "Go to $vault_dir and change values in .env file\nRun docker-compose up -d\nGo to http://localhost:8083 to verify"
}
setup_vaultwarden
echo "Done!"
