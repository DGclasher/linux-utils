# Linux Utils

## Description
Some small scripts to install or setup basic utilities on GNU/Linux

### Contents
+ [Debian Wifi](./debian-wifi) -> Install wifi drivers on debian and based distros.
+ [Samba Debian](./samba-debian) -> Setup SMB server on debian and based distros.
+ [Nextcloud](./nextcloud) -> Setup nextcloud.
+ [Pihole](./pihole) -> Setup pihole for your network.
+ [Vaultwarden](./vaultwarden) -> Self hosted password manager.

