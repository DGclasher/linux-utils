#!/bin/bash

if [ -d "$HOME/pihole" ]; then
    echo "Already exists..."
    exit 1
fi

pihole_dir="$HOME/pihole"

dependencies() {
    sudo apt update
    sudo apt install docker.io -y
    if ! command -v docker-compose; then
        sudo wget "https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)" -O /usr/local/bin/docker-compose
        sudo chmod +x /usr/local/bin/docker-compose
    fi
}
dependencies

pihole_setup() {
    mkdir -p "$pihole_dir/etc-pihole"
    mkdir "$pihole_dir/etc-dnsmasq.d"
    sudo systemctl stop systemd-resolved.service
    sudo systemctl disable systemd-resolved.service
    echo "nameserver 1.1.1.1" | sudo tee /etc/resolv.conf
    wget https://gitlab.com/DGclasher/linux-utils/-/raw/main/pihole/files/.env.pihole -O "$pihole_dir/.env"
    wget https://gitlab.com/DGclasher/linux-utils/-/raw/main/pihole/files/docker-compose.yml -O "$pihole_dir/docker-compose.yml"
}
pihole_setup

configure_firewall() {
    sudo ufw allow 53/tcp
    sudo ufw allow 53/udp
    sudo ufw allow 67/tcp
    sudo ufw allow 67/udp
}
if command -v ufw; then
	configure_firewall
fi

echo -e "Setup done\nGo to $pihole_dir and change the WEBPASSWORD in .env file\nsudo docker-compose up -d\nVisit http://localhost:8082 to see the interface"
