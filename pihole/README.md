# PiHole

## Description
Setup pihole for your network

## Setup
Make the `setup.sh` executable
```
chmod +x setup.sh
```
Execute
```
./setup.sh
```

Change directory to `~/pihole` and change `WEBPASSWORD` to something strong, change `TZ` according to your timezone, refer to [this](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones) for timezones.

Inside `~/pihole` do `sudo docker-compose up -d` to start the containers.

Visit `http://localhost:8082` to see the web interface.