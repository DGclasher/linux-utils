# Install WiFi drivers on Debian

## Description
Script to install wifi drivers in debian and related distros.

### Run
+ Set executable permission to `install.sh`
  ```
  chmod +x install.sh
  ```
+ Execute the script
  ```
  sudo ./install.sh
  ```